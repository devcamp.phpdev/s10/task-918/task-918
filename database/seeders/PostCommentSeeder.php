<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PostCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        for ($i = 1 ; $i <= 1000 ; $i++){
            \DB::table("post_comments")->insert([
                "content" => "Post content {$i}",
                "post_id" => random_int(1,100),
                "user_id" => random_int(1,10)
            ]);
        }
    }
}
