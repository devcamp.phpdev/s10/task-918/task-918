<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class Chuhoa implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        //
        if ($this->kiemTraChuoi($value) == false) {
            $fail('Trường: :attribute . Phải viết hoa chữ đầu tiên');
        }    
    }
    private function kiemTraChuoi($chuoi) {
        $regex = '/^[A-ZÀÁÂẠÃĐÈÉÊẸ̀ÌÍĨỊÒÓÔÕƠƯÚỮÙÛÝỸ][a-záàâạãđèéêẹ̀ìíĩịòóôõơưúữùûýỹ\.]+/';
        if (preg_match($regex, $chuoi)) {
           return true;
        } else {
           return false;
        }
      }
}
