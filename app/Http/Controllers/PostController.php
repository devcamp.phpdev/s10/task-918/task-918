<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\post;
use App\Http\Controllers\Controller;
/*
use App\Http\Requests\StorepostRequest;
use App\Http\Requests\UpdatepostRequest;
*/
use App\Http\Requests\UpdatepostRequest;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $categories = category::all();
        return view('posts.create', ["categories"=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        // die ("Store to DB");
        $request->validate([
            'title' => 'required|max:20',
            'content' => 'required',
            'category_id' => 'integer|min:1',
            'user_id' => 'integer|min:1'
        ]);

        Post::create($request->all() + ["user_id" => \Auth::id() || 1]);
        die("Đã insert vào bảng");
        // return redirect()->route('posts.index')
        //  ->with('success','Post created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        //die ($id);
        $categories = category::all();
        $post = Post::find($id);
        return view('posts.show', ["post"=>$post, "categories"=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatepostRequest $request, string $id)
    {
        //
        $post = Post::find($id);
        $post->update($request->all());
        die ("Đã cập nhật xong");
        /*
        return redirect()->route('posts.index')
            ->with('success', 'Post updated successfully.');
        */
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(post $post)
    {
        //
    }
}
